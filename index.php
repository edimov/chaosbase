<?php
use ChaosBase\Configuration\Configure;
use ChaosBundle\Controller\DefaultController;

use ChaosBaseORM\DBAL\DriverManager;

/**
 * Some constants.
 *
 * DS for shorthand syntax.
 * Directory constants. Mind - no trailing slashes.
 */
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}
if (!defined('CHAOS_BASE_DIR')) {
    define('CHAOS_BASE_DIR', __DIR__);
}
if (!defined('CHAOS_BASE_APP_DIR')) {
    define('CHAOS_BASE_APP_DIR', CHAOS_BASE_DIR . DS . 'app');
}
if (!defined('CHAOS_BASE_CONFIG_DIR')) {
    define('CHAOS_BASE_CONFIG_DIR', CHAOS_BASE_APP_DIR . DS . 'config');
}

require_once(__DIR__ . DS . 'lib' . DS . 'bootstrap.php');

/**
 * Initialize the controller.
 * 
 * @TODO This should be movoed into the bootstrap logic and properly 
 * initiated via routing settings, request and response objects, etc, etc.
 * Let's skip building a complete MVC for now, and focus on the ORM.
 */

$Controller = new DefaultController();
$Controller->indexAction();

// $dbParams = Configure::read('ChaosBaseORM.dbal.default');
// $conn = DriverManager::getConnection($dbParams);
//
// $conn->connect();
// debug($Controller);
// d($conn);

