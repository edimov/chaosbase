<?php
use ChaosBase\Configuration\Configure;

/**
 * Currently supported drivers are:
 *  - pdo_mysql
 */
Configure::write('ChaosBaseORM.dbal.default', array(
    'driver'    => 'pdo_mysql',
    'host'      => 'localhost',
    'dbname'    => 'chaos_base',
    'user'      => 'root',
    'password'  => '',
    'charset'   => 'utf8',
));
