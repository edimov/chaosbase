<?php

namespace ChaosBaseORM\DBAL;

/**
 * Interface DBAL drivers to implement.
 */
interface Driver
{
    /**
     * Attempts to create a connection with the database.
     *
     * @param array
     * @param string|null
     * @param string|null
     *
     * @return \ChaosBaseORM\DBAL\Driver\Connection The database connection.
     */
    public function connect(array $params, $username = null, $password = null);

    /**
     * Gets the name of the driver.
     *
     * @return string
     */
    public function getName();

    /**
     * Gets the name of the database connected to for this driver.
     *
     * @param \ChaosBaseORM\DBAL\Connection $conn
     *
     * @return string The name of the database.
     */
    public function getDatabase(Connection $conn);
}
