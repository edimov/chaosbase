<?php

namespace ChaosBaseORM\DBAL;

final class DriverManager
{
    /**
     * List of supported drivers and their mappings to the driver classes.
     *
     * @var array
     */
    private static $_driverMap = array(
        'pdo_mysql' => 'ChaosBaseORM\DBAL\Driver\PDOMySql\Driver',
    );

    /**
     * DriverManager cannot be instantiated - so private constructor.
     */
    private function __construct()
    {
    }

    /**
     * Create a connection object with given parameters
     *
     * @param array
     *
     * @return \ChasBaseORM\DBAL\Connection
     */
    public static function getConnection(array $params)
    {
        self::_checkParams($params);

        $className = self::$_driverMap[$params['driver']];

        $driver = new $className();

        return new Connection($params, $driver);
    }

    /**
     * Checks if parameters are valid.
     *
     * @param array $params
     *
     * @return void
     *
     * @throws \ErrorException
     */
    private static function _checkParams(array $params)
    {
        if (!isset($params['driver'])) {
            throw new \ErrorException('Missing driver in database config.');
        }
        if (!isset(self::$_driverMap[$params['driver']])) {
            throw new \ErrorException(sprintf('Unsupported driver %s', $params['driver']));
        }
        if (!isset($params['host'])) {
            throw new \ErrorException('Missing host in database config.');
        }
    }
}
