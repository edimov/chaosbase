<?php

namespace ChaosBaseORM\DBAL;

use ChaosBase\Configuration\Configure;

final class ConnectionFactory
{
    /**
     * Array of available connections. Each key represents
     * the ChaosBaseORM.dbal.{key}
     *
     * @var array
     */
    protected static $connections = array();

    /**
     * ConnectionFactory cannot be instantiated
     */
    private function __construct()
    {
    }

    /**
     * @return ChaosBaseORM\DBAL\Connection
     *
     * @throws \Exception
     */
    public static function getConnection($type)
    {
        if (isset(self::$connections[$type])) {
            return self::$connections[$type];
        }

        $params = Configure::read('ChaosBaseORM.dbal.' . $type);
        if (!$params) {
            throw new \Exception(sprintf('Invalid or no settings profived for dbal settings "%s"'), $this->$dbConfig);
        }
        self::$connections[$type] = DriverManager::getConnection($params);

        return self::$connections[$type];
    }
}