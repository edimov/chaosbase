<?php

namespace ChaosBaseORM\DBAL;

use PDO;

/**
 * Well... the real stuff. Connecting etc, etc.
 */
class Connection
{
    /**
     * @var \ChaosBaseORM\DBAL\Driver\Connection
     */
    protected $_conection;

    /**
     * @var \ChaosBaseORM\DBAL\Driver
     */
    protected $_driver;

    /**
     * @var boolean
     */
    private $_connected = false;

    /**
     * @param $params
     * @param \ChaosBaseORM\DBAL\Driver $driver
     */
    public function __construct(array $params, Driver $driver)
    {
        $this->_driver = $driver;
        $this->_params = $params;
    }

    /**
     * Establishes the connection with the database.
     *
     * @return boolean
     */
    public function connect()
    {
        if ($this->_connected) return false;

        $user = isset($this->_params['user']) ? $this->_params['user'] : null;
        $password = isset($this->_params['password']) ? $this->_params['password'] : null;

        $this->_connection = $this->_driver->connect($this->_params, $user, $password);
        $this->_connected = true;

        return true;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * @return boolean
     */
    public function isConnected()
    {
        return $this->_connected;
    }

    /**
     * Closes the connection.
     *
     * @return void
     */
    public function close()
    {
        unset($this->_connection);

        $this->_connected = false;
    }

    /**
     * @param string $statement
     * @param array  $params
     *
     * @return array
     */
    public function fetchAssoc($statement, array $params = array())
    {
        return $this->executeQuery($statement, $params)->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $statement
     * @param array  $params
     *
     * @return array
     */
    public function fetchArray($statement, array $params = array())
    {
        return $this->executeQuery($statement, $params)->fetch(PDO::FETCH_NUM);
    }

    /**
     * Executes an, optionally parametrized, SQL query.
     *
     * @param string $query
     * @param array $params
     *
     * @return PDOStatement
     */
    public function executeQuery($query, array $params = array())
    {
        $this->connect();

        if ($params) {
            // Check whether parameters are positional or named. Mixing is not allowed, just like in PDO.
            if (is_int(key($params))) {
                $stmt = $this->_connection->prepare($query);
            } else {
                $stmt = $this->_connection->prepare($query, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            }
            $stmt->execute($params);
        } else {
            $stmt = $this->_connection->query($query);
        }

        return $stmt;
    }

    /**
     * Executes an SQL UPDATE statement on a table.
     *
     * @param string $tableName
     * @param array  $data
     * @param array  $identifier
     *
     * @return integer The number of affected rows.
     */
    public function update($tableName, array $data, array $identifier)
    {
        $this->connect();
        $set = array();

        foreach ($data as $columnName => $value) {
            $set[] = $columnName . ' = ?';
        }

        $params = array_merge(array_values($data), array_values($identifier));

        $sql  = 'UPDATE ' . $tableName . ' SET ' . implode(', ', $set)
                . ' WHERE ' . implode(' = ? AND ', array_keys($identifier))
                . ' = ?';

        return $this->executeUpdate($sql, $params);
    }

    /**
     * Inserts a table row with specified data.
     *
     * @param string $tableName
     * @param array  $data
     *
     * @return integer The number of affected rows.
     */
    public function insert($tableName, array $data)
    {
        $this->connect();

        if (empty($data)) {
            return $this->executeUpdate('INSERT INTO ' . $tableName . ' ()' . ' VALUES ()');
        }

        $sql = 'INSERT INTO ' . $tableName . ' (' . implode(', ', array_keys($data)) . ')' .
                ' VALUES (' . implode(', ', array_fill(0, count($data), '?')) . ')';

        return $this->executeUpdate(
            $sql,
            array_values($data)
        );
    }

    /**
     * Executes an SQL DELETE statement on a table.
     *
     * @param string $tableName
     * @param array  $identifier
     *
     * @return integer The number of affected rows.
     */
    public function delete($tableName, array $identifier)
    {
        $this->connect();

        $criteria = array();

        foreach (array_keys($identifier) as $columnName) {
            $criteria[] = $columnName . ' = ?';
        }

        $query = 'DELETE FROM ' . $tableName . ' WHERE ' . implode(' AND ', $criteria);

        return $this->executeUpdate($query, array_values($identifier));
    }

    /**
     * Executes an SQL INSERT/UPDATE/DELETE query with the given parameters
     * and returns the number of affected rows.
     *
     * This method supports DBAL mapping types ONLY.
     *
     * @param string $query
     * @param array  $params
     *
     * @return integer The number of affected rows.
     *
     * @throws \Exception
     */
    public function executeUpdate($query, array $params = array())
    {
        $this->connect();

        try {
            if ($params) {
                $stmt = $this->_connection->prepare($query);
                $stmt->execute($params);
                $result = $stmt->rowCount();
            } else {
                $result = $this->_connection->exec($query);
            }
        } catch (\Exception $ex) {
            throw new \Exception('Something went wrong executing the query');
        }

        return $result;
    }

}
