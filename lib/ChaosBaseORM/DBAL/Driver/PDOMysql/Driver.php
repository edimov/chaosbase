<?php

namespace ChaosBaseORM\DBAL\Driver\PDOMySql;

use ChaosBaseORM\DBAL\Driver as DriverInterface;

/**
 * PDO MySql driver.
 */
class Driver implements DriverInterface
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null)
    {
        $conn = new \ChaosBaseORM\DBAL\Driver\PDOConnection(
            $this->_constructPdoDsn($params),
            $username,
            $password
        );

        return $conn;
    }

    /**
     * Constructs the MySql PDO DSN.
     *
     * @param array $params
     *
     * @return string The DSN.
     */
    private function _constructPdoDsn(array $params)
    {
        $dsn = 'mysql:';
        if (isset($params['host']) && $params['host'] != '') {
            $dsn .= 'host=' . $params['host'] . ';';
        }
        if (isset($params['port'])) {
            $dsn .= 'port=' . $params['port'] . ';';
        }
        if (isset($params['dbname'])) {
            $dsn .= 'dbname=' . $params['dbname'] . ';';
        }
        if (isset($params['unix_socket'])) {
            $dsn .= 'unix_socket=' . $params['unix_socket'] . ';';
        }
        if (isset($params['charset'])) {
            $dsn .= 'charset=' . $params['charset'] . ';';
        }

        return $dsn;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'pdo_mysql';
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabase(\ChaosBaseORM\DBAL\Connection $conn)
    {
        $params = $conn->getParams();

        if (isset($params['dbname'])) {
            return $params['dbname'];
        }
        return $conn->query('SELECT DATABASE()')->fetchColumn();
    }
}
