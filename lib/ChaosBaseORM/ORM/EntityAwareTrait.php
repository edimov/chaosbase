<?php

namespace ChaosBaseORM\ORM;

/**
 * Entity aware trait for getting entities
 */
trait EntityAwareTrait
{
    /**
     * Gets entity by its name.
     *
     * @param string $name  Names should be in the format Bundle:EntityName
     *
     * @return Entity
     */
    protected function getEntity($name)
    {
        $className = $this->getEntityClassName($name);

        return new $className();
    }

    /**
     * @param string $name
     *
     * @return string
     *
     * @throws \Exception    
     */
    protected function getEntityClassName($name)
    {
        return EntityNamesMapper::getEntityClassName($name);
    }
}