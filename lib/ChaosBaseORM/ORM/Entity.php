<?php

namespace ChaosBaseORM\ORM;

use ChaosBaseORM\DBAL\ConnectionFactory;

use ChaosBase\Utility\Inflector;
use ChaosBase\Validation\EntityValidator;
use ChaosBase\Validation\ValidationInterface as Validation;
use PDO;

class Entity implements Validation
{
    use EntityRepositoryTrait;

    /**
     * @var string      The table name for this entity
     */
    protected $tableName;

    /**
     * @var string      The alias of the table associated with the Entity
     */
    protected $alias;

    /**
     * Assoc array of fields described
     *
     * @var array
     */
    protected $fields = array();

    /**
     * DBAL config to use. This should be set as:
     *
     * Configure::write('ChaosBaseORM.dbal.{key}', array(
     *      ....
     * ));
     *
     * where {key} is the name of the dbConfig to use.
     */
    protected $dbConfig = 'default';

    /**
     * Database connection this entity uses
     *
     * @var ChaosBaseORM\DBAL\Connection
     */
    private $conn;

    /**
     * Array of validation rules with key => value based setup,
     * where key is camelCased field name
     *
     * @var array
     */
    protected $validate = array();

    /**
     * @var ChaosBase\Validation\Validator
     */
    protected $validator;

    /**
     * @var array
     */
    private $validationErrors;

    /**
     * Constructor.
     *
     * Will set connection, based on Entity settings
     */
    public function __construct()
    {
        $this->conn = ConnectionFactory::getConnection($this->dbConfig);

        if (!$this->tableName) {
            $this->tableName = $this->getTableName();
        }
        if (!$this->alias) {
            $this->alias = $this->getTableAlias();
        }
        if (!$this->fields) {
            $this->fields = $this->getTableFields();
        }
    }

    /**
     * Magic method that takes care of findBy... calls.
     * Valid methods are:
     *      - findByColumnName
     *      - findOneByColumnName
     *      - findByColumnNameAndColumnName
     *      - findOneByColumnNameAndColumnName
     */
    public function __call($method, $args)
    {
        if (is_callable($this, $method)) {
            return call_user_func(array($this, $method));
        }

        if (preg_match('/^find(One)?By([\w]+)$/', $method, $matches)) {
            $columns = explode('And', $matches[2]);
            if (count($args) !== count($columns)) {
                throw new \Exception(sprintf('Invalid number of arguments supplied for %s', $method));
            }
            foreach ($columns as &$column) {
                $column = Inflector::variable($column);
            }
            $findCriteria = array_combine($columns, $args);

            if ($matches[1] === 'One') {
                return $this->findOne($findCriteria);
            } else {
                return $this->find($findCriteria);
            }
        }
        
    }

    /**
     * Get the database table name
     *
     * @return void
     */
    public function getTableName()
    {
        if (!$this->tableName) {
            $reflection = new \ReflectionClass($this);

            $this->tableName = Inflector::pluralize(Inflector::underscore($reflection->getShortName()));
        }

        return $this->tableName;
    }

    /**
     * Get the database table alias
     *
     * @return void
     */
    public function getTableAlias()
    {
        if (!$this->alias) {
            $reflection = new \ReflectionClass($this);

            $this->alias = Inflector::pluralize(Inflector::underscore($reflection->getShortName()));
        }

        return $this->alias;
    }

    /**
     * Get associated table fields with their described info.
     * This can be used in the validator. Currently it's not.
     *
     * @param boolean $force    Whether to force run DESCRIBE again
     * @param null|string $tableName
     * @return array    of described table fields
     *
     * @TODO    Here should be implemented proper caching logic, or external
     *          configs to map the entity fields. This works for the current demo.
     *          Also, what do we do with different drivers?
     *          Either way, preferred than implementing AnnotationParsers.
     */
    public function getTableFields($tableName = null, $force = false)
    {
        if (!$this->fields || $force) {
            $fields = array();

            if (!$tableName) {
                $tableName = $this->tableName ? $this->tableName : $this->getTableName();
            }
            $fieldsRaw = $this->conn->executeQuery("DESCRIBE {$tableName}")->fetchAll(PDO::FETCH_ASSOC);

            foreach ($fieldsRaw as $field) {
                $fieldData = array(
                    'type' => $field['Type'],
                    'len' => '',
                    'flags' => array(),
                    'tableIdenitier' => false,
                );
                if (preg_match("|([a-z]+)\(([0-9]+)\).*|iU", $field['Type'], $m)) {
                    $fieldData['type'] = $m[1];
                    $fieldData['len'] = $m[2];
                }
                if (strtolower($field['Null']) === 'no') {
                    $fieldData['flags'][] = 'not_null';
                }
                if (strpos($field['Key'], 'PRI') !== false) {
                    $fieldData['flags'][] = 'primary_key';
                    $fieldData['tableIdenitier'] = true;
                }
                $fieldData['flags'] = implode(' ', $fieldData['flags']);
                $fields[$field['Field']] = $fieldData;
            }

            $this->fields = $fields;
        }

        return $this->fields;
    }

    /**
     * Returns populted entity columns with values as associated array.
     * Really contemplated ArrayAccess... but come on, ArrayAccess on Entity...
     *
     * @return array
     */
    public function getEntityFieldsArr()
    {
        $fields = array();
        foreach ($this->fields as $col => $data) {
            $getter = 'get' . Inflector::camelize($col);
            $fields[$col] = $this->$getter();
        }
        return $fields;
    }

    /**
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function getQueryBuilder()
    {
        return new QueryBuilder($this->conn);
    }

    /**
     * @return ChaosBase\Validation\Validator
     */
    public function getValidator()
    {
        if (!$this->validator) {
            $this->validator = new EntityValidator($this);
        }
        return $this->validator;
    }

    /**
     * ValidationInterface.
     * Pass validation array to validator.
     *
     * @return array
     */
    public function getValidationRules()
    {
        return $this->validate;
    }

    /**
     * ValidationInterface.
     * Set validation errors.
     *
     * @param array
     *
     * @return array
     */
    public function setValidationErrors(array $errors)
    {
        $this->validationErrors = $errors;
    }

    /**
     * ValidationInterface.
     * Get validation errors.
     *
     * @return array
     */
    public function getValidationErrors()
    {
        return $this->validationErrors;
    }

    /**
     * Destructor. Closes the connection.
     */
    public function __destruct()
    {
        $this->conn->close();
    }

    // ========================== //
    // LIFECYDLE CALLBACK METHODS //
    // ========================== //

    /**
     * Now, this needs proper event managment implementation.
     * This is not the best, but does the job for autouprate
     * created and modified fields if any.
     */

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
    }
}
