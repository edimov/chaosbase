<?php

namespace ChaosBaseORM\ORM;

use ChaosBase\Utility\Inflector;
use ChaosBaseORM\ORM\QueryBuilder;
use PDO;

/**
 * Since Entity should act as a repository. Trait is not the
 * most straight forward way to approach this need, but for
 * such a small project - makes sense. When expanding the trait
 * could be used by a parent class to inherit the basic functionalities.
 */
trait EntityRepositoryTrait
{
    /**
     * Finds row/rows by the given criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The objects.
     */
    public function find(array $criteria = array(), array $orderBy = null, $limit = null, $offset = null)
    {
        $entityName = EntityNamesMapper::reverseEntityClassName(get_class($this));

        $res = $this->getQueryBuilder()
            ->select($this->alias)
            ->from($entityName, $this->alias)
            ->whereCriteria($criteria, $this->alias)
            ->orderBy($this->_prefixOrderStatements($orderBy))
            ->limit($limit, $offset)
            ->getQuery()
            ->getResults();
        return $res;
    }

    /**
     * Finds row by the given criteria.
     *
     * @param array $criteria
     * @param array|null $orderBy
     *
     * @return array The object.
     */
    public function findOne(array $criteria = array(), $orderBy = null)
    {
        $res = $this->find($criteria, $orderBy, 1);

        return $res ? reset($res) : false;
    }

    /**
     * Finds all rows matching the given criteria.
     * Not a lot of sence here, since find will do this,
     * But let's stick to the requirements.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The object.
     */
    public function findAll(array $criteria = array(), array $orderBy = null, $limit = null, $offset = null)
    {
        $res = $this->find($criteria, $orderBy, $limit, $offset);
        return $res;
    }

    /**
     * Inserts, or Updates the Entity. Logic is based on the tablaIdentifier
     * field, fetched via DESCRIBE query, when Entity is instantiated.
     *
     * @return integer
     */
    public function save()
    {
        extract($this->_getIdentifierFields(), EXTR_OVERWRITE);

        $res = false;

        if ($this->getValidator()->validates()) {
            if (!$identifier) {
                $this->onPrePersist();
                extract($this->_getIdentifierFields(), EXTR_OVERWRITE);

                $data = array_filter($data);
                $res = $this->conn->insert($this->tableName, $data);
            } else {
                $this->onPreUpdate();
                extract($this->_getIdentifierFields(), EXTR_OVERWRITE);

                $res = $this->conn->update($this->tableName, $data, $identifier);
            }
        }

        return $res;
    }

    /**
     * Deletes the table row (entity's representation) from the DB.
     *
     * @return integer
     */
    public function delete()
    {
        extract($this->_getIdentifierFields(), EXTR_OVERWRITE);

        return $this->conn->delete($this->tableName, $identifier);
    }

    /**
     * Gets an array with identifier fields for the Entity,
     * used in the Connection wrapper
     *
     * @return array    Assoc array with "data" and "identifier" keys
     */
    private function _getIdentifierFields()
    {
        $entityData = $this->getEntityFieldsArr();

        $data = array();
        $identifier = array();
        foreach ($this->fields as $field => $props) {
            if ($props['tableIdenitier']) {
                if ($entityData[$field]) {
                    $identifier[$field] = $entityData[$field];
                }
                continue;
            }
            $data[$field] = $entityData[$field];
        }

        return array('data' => $data, 'identifier' => $identifier);
    }

    /**
     * @param array|null $orderBy
     * @param string|null $alias    to prefix with
     */
    private function _prefixOrderStatements($orderBy, $alias = null)
    {
        if (!$alias) {
            $alias = $this->alias;
        }

        $res = array();
        if ($orderBy && is_array($orderBy)) {
            foreach ($orderBy as $k => $v) {
                if (is_string($v)) {
                    $res[$alias . '.' . $k] = $v;
                } elseif (is_array($v)) {
                    $_res = array();
                    foreach ($v as $__k => $__v) {
                        $_res[$alias . '.' . $__k] = $__v;
                    }
                    $res[] = $_res;
                }
            }
        }
        return $res;
    }
}
