<?php

namespace ChaosBaseORM\ORM;

use ChaosBase\Utility\Inflector;

class QueryResultMapper
{
    /**
     * @var array   with aliases => Entity names
     */
    private $aliases;

    /**
     * @var array   with results
     */
    private $results;

    /**
     * @var array with mapped entities
     */
    private $mappedResults = array();

    /**
     * @param array $aliases
     * @param array $results
     */
    public function __construct(array $aliases, array $results)
    {
        $this->aliases = $aliases;
        $this->results = $this->_splitResults($results);
    }

    /**
     * Gets mapped results to entities.
     *
     * @return mixed array|false on error
     */
    public function getResults()
    {
        if (!$this->results) {
            return false;
        }


        foreach ($this->results as $result) {
            foreach ($result as $alias => $data) {
                $entity = $this->_getEntityForAlias($alias);
                foreach ($data as $field => $value) {
                    $callable = array($entity, 'set' . Inflector::camelize($field));
                    if (is_callable($callable)) {
                        call_user_func_array($callable, array($value));
                    }
                }
                $this->mappedResults[] = $entity;
            }
        }

        return $this->mappedResults;
    }

    /**
     * Arranges the raw SQL results into array with 'alias' => array structure.
     *
     * @param array $results    raw sql results
     *
     * @return array
     */
    private function _splitResults(array $results)
    {
        $_results = array();
        foreach ($results as $result) {
            $_res = array();
            foreach ($result as $col => $val) {
                $parts = explode('_', $col);
                $entityAlias = array_shift($parts);
                $colName = implode('_', $parts);

                if (!isset($_res[$entityAlias])) {
                    $_res[$entityAlias] = array();
                }
                $colFriendlyName = Inflector::variable(strtolower($colName));
                $_res[$entityAlias][$colFriendlyName] = $val;
            }
            $_results[] = $_res;
        }

        return $_results;
    }

    /**
     * Gets mapped entity by passed alias
     *
     * @return Entity
     *
     * @throws \NotFoundException
     */
    private function _getEntityForAlias($alias)
    {
        if (isset($this->aliases[$alias])) {
            $entityClassName = EntityNamesMapper::getEntityClassName($this->aliases[$alias]);

            return new $entityClassName();
        }

        throw new \NotFoundException('Error retrieving entity from alias. Please check your query.');
    }

}
