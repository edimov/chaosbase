<?php

namespace ChaosBaseORM\ORM;

use ChaosBaseORM\ORM\QueryResultMapper;
use ChaosBase\Utility\Inflector;
use PDO;

class QueryBuilder
{
    /**
     * @var \ChaosBaseORM\DBAL\Connection
     */
    private $conn;

    /**
     * @var array   parts for the SQL query
     */
    private $parts;

    /**
     * @var array   with aliases and entities
     */
    private $selects = array();

    /**
     * @var array where clauses
     */
    private $conditions = array();

    /**
     * @var array order as key => value pair
     */
    private $order = array();

    /**
     * @var array limit as 'limit' => num, 'offset' => num
     */
    private $limit = array();

    /**
     * @var arrya
     */
    private $parameters = array();

    /**
     * Compiled SQL statement
     *
     * @var string
     */
    private $sqlQuery = '';

    /**
     * @var array
     */
    private $resultsRaw;

    /**
     * Constructor.
     *
     * @param \ChaosBaseORM\DBAL\Connection $conn      The connection to run queries on
     */
    public function __construct(\ChaosBaseORM\DBAL\Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param Entity $from
     * @param string $alias
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function select($alias)
    {
        $this->selects = array($alias => '');

        return $this;
    }

    /**
     * Not really handy to call only one "from" statement...
     *
     * @param string $entityName
     * @param string $alias
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     *
     * @throws InvalidArgumentException
     */
    public function from($entityName, $alias)
    {
        if (!isset($this->selects[$alias])) {
            throw new \InvalidArgumentException(sprintf('Unknown alias %s for the query.', $alias));
        }
        $this->selects[$alias] = $entityName;

        return $this;
    }

    /**
     * @param string $where     where condition
     */
    public function where($where)
    {
        $this->conditions[] = $where;

        return $this;
    }

    /**
     * @param string $where     and where condition
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function andWhere($where)
    {
        $this->conditions[] = $where;

        return $this;
    }

    /**
     * @param array $criteria   arrayed criteria to parse
     * @param string $alias
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function whereCriteria(array $criteria, $alias = null)
    {
        foreach ($criteria as $col => $val) {
            $this->conditions[] = $this->_getComoparisonExpression($col, $val, $alias);
        }

        return $this;
    }

    /**
     * @param mixed string|array $orderby
     * @param string $order     Here proper implementation
     *                          with constants would be great...
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function orderBy($orderby = null, $oder = 'ASC')
    {
        if ($orderby) {
            if (is_string($orderby)) {
                $this->order[$orderby] = $order;
            } elseif (is_array($orderby)){
                foreach ($orderby as $k => $v) {
                    if (is_array($v)) {
                        foreach ($v as $__k => $__v) {
                            $this->order[$__k] = $__v;
                        }
                    } else {
                        $this->order[$k] = $v;
                    }
                }
            }
        }

        return $this;
    }

    /**
     * @param integer $limit 
     * @param integer $offset 
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function limit($limit, $start = 0)
    {
        $this->limit = array(
            'limit' => (int) $limit,
            'offset' => $start
        );

        return $this;
    }

    /**
     * Gets the built query as string
     *
     * @return string
     */
    public function getQuery()
    {
        $query = 'SELECT';

        // Now, this is really disgusting, but the ORM for the time being
        // needs to work with a single table only, so... it shall stay.
        // Can be improved easily in this class.

        $selectsAliases = array_keys($this->selects);
        $selectEntityNames = array_values($this->selects);
        $selectAlias = reset($selectsAliases);
        $selectEntityName = reset($selectEntityNames);
        $selectEntityClassName = EntityNamesMapper::getEntityClassName($selectEntityName);

        $selectEntity = new $selectEntityClassName();

        $query .= ' ' . $this->_getSelectFieldsSQL($selectEntity->getTableFields(), $selectAlias) . "\n";
        $query .= 'FROM ' . $selectEntity->getTableName() . ' ' . $selectAlias . "\n";

        $query .= $this->_getWhereConditions(). "\n";
        $query .= $this->_getOrderBySQL(). "\n";
        $query .= $this->_getLimitSQL(). "\n";

        $this->sqlQuery = $query;

        return $this;
    }

    /**
     * @return array    associative array from query
     */
    public function getResults()
    {
        $results = false;

        $q = $this->conn->executeQuery($this->sqlQuery, $this->parameters);
        if ($q) {
            $this->resultsRaw = $q->fetchAll(PDO::FETCH_ASSOC);
            $results = $this->getResultMapper()->getResults();
        }

        return $results;
    }

    /**
     * @var string $key
     * @var string $value
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function setParam($key, $value)
    {
        $this->parameters[$key] = $value;
    }

    /**
     * @param array $parameters     Parameters for the query
     *
     * @return ChaosBaseORM\ORM\QueryBuilder
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * @return QueryResultMapper
     */
    protected function getResultMapper()
    {
        return new QueryResultMapper($this->selects, $this->resultsRaw);
    }

    /**
     * @var string $col
     * @var string val
     * @var string $alias
     */
    private function _getComoparisonExpression($col, $val, $alias = null)
    {
        $dbColumnName = strtolower(Inflector::underscore($col));
        $expr = $alias . '.' . $dbColumnName;

        if (preg_match("'(LIKE|>|<|<=|>=)\s(.*)$'iU", $val, $m)) {
            $expr .= ' ' . $m[1] . ' :' . $col;;
            $this->setParam($col, $m[2]);
        } else {
            $expr .= ' = :' . $col;
            $this->setParam($col, $val);
        }

        return $expr;
    }

    /**
     * @return string
     */
    private function _getWhereConditions()
    {
        $sql = '';
        if ($this->conditions) {
            $sql = ' WHERE ' . implode(' AND ', $this->conditions) . "\n";
        }
        return $sql;
    }

    /**
     * @return string
     */
    private function _getSelectFieldsSQL(array $fields, $alias)
    {
        $sqlFields = array();
        foreach ($fields as $field => $params) {
            $sqlFields[] = $alias . '.' . $field . ' as ' . $alias . '_' . $field;
        }

        return implode(', ', $sqlFields);
    }

    /**
     * @return string
     */
    private function _getOrderBySQL()
    {
        $_orderStatements = array();
        foreach($this->order as $column => $dir) {
            $column = strtolower(Inflector::underscore($column));

            $_orderStatements[] = $column . ' ' . strtoupper($dir);
        }

        $sql = '';
        if ($_orderStatements) {
            $sql = 'ORDER BY ' . implode(', ', $_orderStatements);
        }

        return $sql;
    }

    /**
     * @return string
     */
    private function _getLimitSQL()
    {
        $sql = '';

        if ($this->limit && isset($this->limit['limit']) && $this->limit['limit']) {
            $sql = 'LIMIT ' . (int) $this->limit['limit'];
            if (isset($this->limit['offset']) && $this->limit['offset']) {
                $sql .= ', ' . (int) $this->limit['offset'];
            }
        }
        
        return $sql;
    }

}
