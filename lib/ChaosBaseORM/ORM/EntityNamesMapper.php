<?php

namespace ChaosBaseORM\ORM;

class EntityNamesMapper
{
    /**
     * Get entity class name from 'Bundle:Entity' string
     *
     * @param string $name
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function getEntityClassName($name)
    {
        $parts = explode(':', $name);
        $partsCount = count($parts);

        switch ($partsCount) {
            case 1:
                $className = $name;
                break;
            case 2:
                $className = $parts[0] . '\\Entity\\' . $parts[1];
                break;
            default:
                throw new \Exception('Invalid name argument for the entity');
        }

        return $className;
    }

    /**
     * Get entity 'Bundle:Entity' string name from class name
     *
     * @param string $name
     *
     * @return string
     *
     * @throws \Exception
     */
    public static function reverseEntityClassName($className)
    {
        $parts = explode('\\', $className);
        $partsCount = count($parts);

        switch ($partsCount) {
            case 1:
                $fwName = $className;
                break;
            case 2:
                $fwName = $parts[0] . ':' . $parts[1];
                break;
            case 3:
                $fwName = $parts[0] . ':' . $parts[2];
                break;
            default:
                throw new \Exception('Invalid name argument for the entity');
        }

        return $fwName;
    }

}
