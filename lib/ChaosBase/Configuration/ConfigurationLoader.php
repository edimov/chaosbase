<?php

namespace ChaosBase\Configuration;

class ConfigurationLoader
{

    /**
     * Very simple method for loading configurations from the app.
     * Should be rewritten, but for the ORM purposes - enough.
     *
     * @return void
     */
    public static function load()
    {
        $dbConfigFile = CHAOS_BASE_CONFIG_DIR . DS . 'database_config.php';
        if (!is_file($dbConfigFile)) {
            trigger_error("No configuration present. Please create the file {$dbConfigFile}");
        }
        require_once(CHAOS_BASE_CONFIG_DIR . DS . 'database_config.php');
    }
}