<?php

namespace ChaosBase\Validation;

use ChaosBase\Utility\Inflector;
use ChaosBaseORM\ORM\Entity as Entity;

class EntityValidator extends Validator
{
    /**
     * @var Entity
     */
    private $entity;

    /**
     * @var array
     */
    protected $errors = array();

    /**
     * @var array
     */
    protected $defaultErrorMessages = array(
        'notEmpty' => 'Field %s cannot be empty.',
        'number' => 'Invalid number provided for field %s.',
        'length' => 'Invalid length of field %s.',
        'date' => 'Invalid date for field %s.',
        'email' => 'Invalid email.',
        'ip' => 'Invalid ip.',
    );

    /**
     * @param Entity $entity
     */
    public function __construct(Entity $entity)
    {
        $this->entity = $entity;

        $this->setRules($entity->getValidationRules());
    }

    /**
     * Checks if rules passed in config validate.
     *
     * @return boolean
     */
    public function validates()
    {
        if (!$this->rules) {
            return true;
        }

        foreach ($this->rules as $colName => $_rule) {
            $fieldValue = $this->_getFieldValue($colName);
            if (isset($_rule['rule'])) {
                $_rule = array($_rule);
            }

            foreach ($_rule as $singleRule) {

                if (!isset($singleRule['rule'])) {
                    continue;
                }

                if (!isset($this->rulesMap[$singleRule['rule']])) {
                    continue;
                }
                if (!$this->validateRule($singleRule, $fieldValue)) {
                    $this->_setError($colName, $singleRule);
                }
            }
        }

        $this->entity->setValidationErrors($this->errors);

        return empty($this->errors);
    }

    /**
     * Gets the field value from the entity
     *
     * @return mixed
     */
    private function _getFieldValue($fieldName)
    {
        $val = false;

        $method = 'get' . Inflector::camelize($fieldName);
        if (is_callable(array($this->entity, $method))) {
            $val = call_user_func(array($this->entity, $method));
        }

        return trim($val);
    }

    /**
     * @var string $fieldName
     * @var array $rule
     *
     * @return void
     */
    private function _setError($fieldName, array $rule)
    {
        $msg = 'Uknown error';
        if (isset($rule['message'])) {
            $msg = $rule['message'];
        } elseif (isset($this->defaultErrorMessages[$rule['rule']])) {
            $msg = sprintf($this->defaultErrorMessages[$rule['rule']], $fieldName);
        }
        if (!isset($this->errors[$fieldName])) {
            $this->errors[$fieldName] = array();
        }

        $this->errors[$fieldName][] = $msg;
    }

}