<?php

namespace ChaosBase\Validation;

interface ValidationInterface
{
    /**
     * Get array of validation rules.
     *
     * @return array
     */
    public function getValidationRules();

    /**
     * Get array of validation rules.
     *
     * @param array $errors
     *
     * @return array
     */
    public function setValidationErrors(array $errors);

    /**
     * Get array of validation rules.
     *
     * @return array
     */
    public function getValidationErrors();
}
