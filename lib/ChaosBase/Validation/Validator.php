<?php

namespace ChaosBase\Validation;

class Validator
{

    /**
     * Array of rule names and methods
     *
     * @var array
     */
    protected $rulesMap = array(
        'notEmpty' => 'validateNotEmpty',
        'number' => 'validateNumber',
        'length' => 'validateLength',
        'date' => 'validateDate',
        'email' => 'validateEmail',
        'ip' => 'validateIp',
    );

    /**
     * @var array
     */
    protected $rules = array();

    /**
     * Gets array with validation rules.
     *
     * @param array
     */
    protected function setRules($rules)
    {
        $this->rules = $rules;
    }

    /**
     * @return array
     */
    protected function validateRule($rule, $val)
    {
        if (!isset($rule['rule'])) {
            return false;
        }
        if (!isset($this->rulesMap[$rule['rule']])) {
            return false;
        }

        $method = $this->rulesMap[$rule['rule']];
        return call_user_func_array(array($this, $method), array($val, $rule));
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateNotEmpty($val, array $params = array())
    {
        $val = preg_replace('/\s+/', '', $val);

        return !empty($val);
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateNumber($val, array $params = array())
    {
        return is_numeric($val);
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateLength($val, array $params = array())
    {
        $params = array_merge(array(
            'settings' => array(
                'min' => 0,
                'max' => 120,
            )
        ), $params);

        $min = isset($params['settings']['min']) ? $params['settings']['min'] : 0;
        $max = isset($params['settings']['max']) ? $params['settings']['min'] : 120;

        $strlen = strlen($val);
        return $strlen >= $min && $strlen <= $max;
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateDate($val, array $params = array())
    {
        return \DateTime::createFromFormat('Y-m-d G:i:s', $val) !== false;
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateEmail($val, array $params = array())
    {
        return (bool) filter_var($val, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @var string $val
     * @var array $params
     */
    public function validateIp($val, array $params = array())
    {
        return (bool) filter_var($val, FILTER_VALIDATE_IP);
    }

}
