<?php

namespace ChaosBase\Controller;

use ChaosBaseORM\ORM\EntityAwareTrait;

/**
 * Simple Controller logic as the 'C' in MVC. Used for proper structure only.
 */
class Controller
{
    use EntityAwareTrait;

}
