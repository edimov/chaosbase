<?php
use ChaosBase\Configuration\ConfigurationLoader;

/**
 * Some basics
 */
require_once __DIR__ . DS . 'basics.php';

/**
 * Let's init the autoloader, thanks to composer
 */
$loader = require_once __DIR__ . DS . '..' . DS . 'vendor' . DS . 'autoload.php';

ConfigurationLoader::load();
