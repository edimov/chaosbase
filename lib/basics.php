<?php

use ChaosBase\Utility\Debugger;

if (!function_exists('d')) {
    /**
     * Prints out debug information about given variable and exits script.
     *
     * @param boolean $var Variable to show debug information for.
     */
    function d($var) {
        debug($var, null, true, 2, 3);
        exit;
    }
}

if (!function_exists('debug')) {
    /**
     * Prints out debug information about given variable.
     *
     * @param boolean $var Variable to show debug information for.
     * @param boolean $showHtml If set to true, the method prints the debug data in a browser-friendly way.
     * @param boolean $showFrom If set to true, the method prints from where the function was called.
     * @param int $startFrom Which trace to start debugging from
     * @param int $depth Depth for debugger
     */
    function debug($var = false, $showHtml = null, $showFrom = true, $startFrom = 1, $depth = 2) {
        $file = '';
        $line = '';
        $lineInfo = '';
        if ($showFrom) {
            $trace = Debugger::trace(array('start' => $startFrom, 'depth' => $depth, 'format' => 'array'));
            $file = $trace[0]['file'];
            // $file = str_replace(array(BLUEBOXFW_CORE_INCLUDE_PATH, BLUEBOXFW_ROOT), '', $trace[0]['file']);
            $line = $trace[0]['line'];
        }
        $html = <<<HTML
<div class="debug-output">
%s
<pre class="debug">
%s
</pre>
</div>
HTML;
        $text = <<<TEXT
%s
########## DEBUG ##########
%s
###########################
TEXT;
        $template = $html;
        if (php_sapi_name() == 'cli' || $showHtml === false) {
            $template = $text;
            if ($showFrom) {
                $lineInfo = sprintf('%s (line %s)', $file, $line);
            }
        }
        if ($showHtml === null && $template !== $text) {
            $showHtml = true;
        }
        $var = Debugger::exportVar($var, 25);
        if ($showHtml) {
            $template = $html;
            $var = htmlentities($var);
            if ($showFrom) {
                $lineInfo = sprintf('<span><strong>%s</strong> (line <strong>%s</strong>)</span>', $file, $line);
            }
        }
        printf($template, $lineInfo, $var);
    }
}
