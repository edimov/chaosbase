# README #

This is a simple ORM attempt. It implements some well known design patterns.

### Chaos Test Task ###

* Simple ORM

### How do I get set up? ###

You'd need to import the sqldump (it's inside the sqldump folder) to get started.

### What does it do? ###

The functionality of the basic ORM is quite straight forward. You can do CRUD operations. A huge limitation I wanted to overcome is the lack of mysql PDO wrapper, but it would have taken ages to implement.

It uses composer for autoloading and good stuff to come.

Currently the ORM works with pdo_mysql driver only.

## How to config DB? ##

Configuration is done via a singleton Configure class.

```
#!php

Configure::write('ChaosBaseORM.dbal.default', array(
    'driver'    => 'pdo_mysql',
    'host'      => 'localhost',
    'dbname'    => 'chaos_base',
    'user'      => 'root',
    'password'  => '',
    'charset'   => 'utf8',
));
```


### How to use? ###

Queries are done in the following manner from the controller:


```
#!php
        $entity = $this->getEntity('ChaosBundle:Entity')
            ->findAll(
                array(
                    'camleCasedCol' => 'LIKE %po%',
                    'secondCol' => '> 6000',
                ), array(
                    'OrderByCol' => 'ASC'
                )
        );

```

You could use a QueryBuilder interface as well, from any Entity. This should be available for the Controller as well... some day.

```
#!php
$this->getQueryBuilder()
            ->select($alias)
            ->from($entityName, $alias)
            ->whereCriteria($criteria, $this->alias)
            ->orderBy($orderBy)
            ->limit($limit, $offset)
            ->getQuery()
            ->getResults();

```

### The database ###

A MySQLWorkbench EER diagram.

![ChaosBaseEER.png](https://bitbucket.org/repo/74d87E/images/4027838301-ChaosBaseEER.png)