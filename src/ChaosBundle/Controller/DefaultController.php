<?php

namespace ChaosBundle\Controller;

use ChaosBase\Controller\Controller;
use ChaosBundle\Entity\Address;
use ChaosBundle\Entity\City;
use ChaosBundle\Entity\Company;
use ChaosBundle\Entity\Country;
use ChaosBundle\Entity\Employee;
use ChaosBundle\Entity\Order;
use ChaosBundle\Entity\OrderItem;

class DefaultController extends Controller
{

    public function indexAction()
    {
        $cities = $this->getEntity('ChaosBundle:City')
            ->findAll(
                array(
                    'slug' => 'LIKE %po%',
                    'postCode' => '> 6000',
                ), array(
                    'phoneCode' => 'ASC'
                )
        );

        debug($cities);

        $company = $this->getEntity('ChaosBundle:Company')
            ->findOneByName('Chaos Group');
        if (!$company) {
            $company = new Company();
            $company->setName('Chaos Group');
            
            // If this is commented, save will fail, as defined in the Entity
            // validation props
            //
            // $company->setEmail('chaos@chaosgroup.com');

            if ($company->save()) {
                debug('Company saved succesfully');
            } else {
                debug('Validation errors occured. Please check "ChaosBundle:Company" entity.:');
                debug($company->getValidationErrors());
            }
        } else {
            $company->setPhone(rand());
            $company->save();
        }

        debug($company);
    }
}
