<?php

namespace ChaosBundle\Entity;

use ChaosBaseORM\ORM\Entity as Entity;

/**
 * Addresses
 */
class Address extends Entity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $address;

    /**
     * @var integer
     */
    private $countryId;

    /**
     * @var integer
     */
    private $cityId;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * {@inheritdoc}
     */
    protected $validate = array(
        'address' => array(
            array(
                'rule' => 'notEmpty',
            ),
            array(
                'rule' => 'length',
                'message' => 'Field cannot be less than 4 and more than 120 charctars long.',
                'settings' => array(
                    'min' => 4,
                    'max' => 120
                ),
            ),
        ),
        'countryId' => array(
            array(
                'rule' => 'number',
            ),
            array(
                'rule' => 'notEmpty',
            ),
        ),
        'cityId' => array(
            array(
                'rule' => 'number',
            ),
            array(
                'rule' => 'notEmpty',
            ),
        ),
    );


    /**
     * Set id
     *
     * @param int $id
     * @return City
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Addresses
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set countryId
     *
     * @param integer $countryId
     *
     * @return Addresses
     */
    public function setCountryId($countryId)
    {
        $this->countryId = $countryId;

        return $this;
    }

    /**
     * Get countryId
     *
     * @return integer
     */
    public function getCountryId()
    {
        return $this->countryId;
    }

    /**
     * Set cityId
     *
     * @param integer $cityId
     *
     * @return Addresses
     */
    public function setCityId($cityId)
    {
        $this->cityId = $cityId;

        return $this;
    }

    /**
     * Get cityId
     *
     * @return integer
     */
    public function getCityId()
    {
        return $this->cityId;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Addresses
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return Addresses
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Addresses
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Addresses
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $now = date('Y-m-d H:i:s');
        $this->created = $now;
        $this->modified = $now;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modified = date('Y-m-d H:i:s');
    }
}

