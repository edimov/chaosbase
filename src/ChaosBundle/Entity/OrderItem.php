<?php

namespace ChaosBundle\Entity;

use ChaosBaseORM\ORM\Entity as Entity;

/**
 * OrderItems
 */
class OrderItem extends Entity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $orderId;

    /**
     * @var integer
     */
    private $productId;

    /**
     * @var string
     */
    private $unitPrice;

    /**
     * @var string
     */
    private $unitDiscount;

    /**
     * @var integer
     */
    private $unitQuantity;


    /**
     * Set id
     *
     * @param int $id
     * @return City
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set orderId
     *
     * @param integer $orderId
     *
     * @return OrderItems
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;

        return $this;
    }

    /**
     * Get orderId
     *
     * @return integer
     */
    public function getOrderId()
    {
        return $this->orderId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     *
     * @return OrderItems
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set unitPrice
     *
     * @param string $unitPrice
     *
     * @return OrderItems
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * Get unitPrice
     *
     * @return string
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * Set unitDiscount
     *
     * @param string $unitDiscount
     *
     * @return OrderItems
     */
    public function setUnitDiscount($unitDiscount)
    {
        $this->unitDiscount = $unitDiscount;

        return $this;
    }

    /**
     * Get unitDiscount
     *
     * @return string
     */
    public function getUnitDiscount()
    {
        return $this->unitDiscount;
    }

    /**
     * Set unitQuantity
     *
     * @param integer $unitQuantity
     *
     * @return OrderItems
     */
    public function setUnitQuantity($unitQuantity)
    {
        $this->unitQuantity = $unitQuantity;

        return $this;
    }

    /**
     * Get unitQuantity
     *
     * @return integer
     */
    public function getUnitQuantity()
    {
        return $this->unitQuantity;
    }
}

