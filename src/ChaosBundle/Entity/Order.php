<?php

namespace ChaosBundle\Entity;

use ChaosBaseORM\ORM\Entity as Entity;

/**
 * Orders
 */
class Orders extends Entity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyId;

    /**
     * @var integer
     */
    private $shippingAddressId;

    /**
     * @var integer
     */
    private $billingAddressId;

    /**
     * @var string
     */
    private $totalPrice;

    /**
     * @var string
     */
    private $notes;

    /**
     * @var integer
     */
    private $ip;

    /**
     * @var \DateTime
     */
    private $created;

    /**
     * @var \DateTime
     */
    private $modified;

    /**
     * {@inheritdoc}
     */
    protected $validate = array(
        'name' => array(
            'rule' => 'notEmpty',
        ),
        'email' => array(
            array(
                'rule' => 'notEmpty',
            ),
            array(
                'rule' => 'email',
            ),
        ),
    );


    /**
     * Set id
     *
     * @param int $id
     * @return City
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyId
     *
     * @param integer $companyId
     *
     * @return Orders
     */
    public function setCompanyId($companyId)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return integer
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set shippingAddressId
     *
     * @param integer $shippingAddressId
     *
     * @return Orders
     */
    public function setShippingAddressId($shippingAddressId)
    {
        $this->shippingAddressId = $shippingAddressId;

        return $this;
    }

    /**
     * Get shippingAddressId
     *
     * @return integer
     */
    public function getShippingAddressId()
    {
        return $this->shippingAddressId;
    }

    /**
     * Set billingAddressId
     *
     * @param integer $billingAddressId
     *
     * @return Orders
     */
    public function setBillingAddressId($billingAddressId)
    {
        $this->billingAddressId = $billingAddressId;

        return $this;
    }

    /**
     * Get billingAddressId
     *
     * @return integer
     */
    public function getBillingAddressId()
    {
        return $this->billingAddressId;
    }

    /**
     * Set totalPrice
     *
     * @param string $totalPrice
     *
     * @return Orders
     */
    public function setTotalPrice($totalPrice)
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    /**
     * Get totalPrice
     *
     * @return string
     */
    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Orders
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set ip
     *
     * @param integer $ip
     *
     * @return Orders
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return integer
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return Orders
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return Orders
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $now = date('Y-m-d H:i:s');
        $this->created = $now;
        $this->modified = $now;
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->modified = date('Y-m-d H:i:s');
    }
}
