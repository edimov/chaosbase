# ************************************************************
# Sequel Pro SQL dump
# Version 4499
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.1.48)
# Database: chaos_base
# Generation Time: 2016-01-04 11:48:57 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table addresses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `addresses`;

CREATE TABLE `addresses` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `country_id` int(11) NOT NULL,
  `city_id` int(11) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table cities
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(254) NOT NULL DEFAULT '',
  `slug` varchar(254) DEFAULT '',
  `country_id` int(11) NOT NULL,
  `post_code` varchar(100) DEFAULT '',
  `phone_code` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;

INSERT INTO `cities` (`id`, `name`, `slug`, `country_id`, `post_code`, `phone_code`)
VALUES
	(1,'Айтос','aytos',33,'8500','+558'),
	(2,'Аксаково','aksakovo',33,'9154','+52'),
	(3,'Алфатар','alfatar',33,'7570','+8673'),
	(4,'Антоново','antonovo',33,'7970','+6071'),
	(5,'Априлци','apriltsi',33,'5641','+6958'),
	(6,'Ардино','ardino',33,'6750','+3651'),
	(7,'Асеновград','asenovgrad',33,'4230','+331'),
	(8,'Ахелой','aheloy',33,'8217','+596'),
	(9,'Ахтопол','ahtopol',33,'8280','+590'),
	(10,'Балчик','balchik',33,'9600','+579'),
	(11,'Банкя','bankya',33,'1320','+2997'),
	(12,'Банско','bansko',33,'2770','+749'),
	(13,'Баня','banya',33,'4360','+3132'),
	(14,'Батак','batak',33,'4580','+3553'),
	(15,'Батановци','batanovtsi',33,'2340','+7712'),
	(16,'Белене','belene',33,'5930','+658'),
	(17,'Белица','belitsa',33,'2780','+7444'),
	(18,'Белово','belovo',33,'4470','+3581'),
	(19,'Белоградчик','belogradchik',33,'3900','+936'),
	(20,'Белослав','beloslav',33,'9178','+5112'),
	(21,'Берковица','berkovitsa',33,'3500','+953'),
	(22,'Благоевград','blagoevgrad',33,'2700','+73'),
	(23,'Бобов дол','bobov-dol',33,'2670','+702'),
	(24,'Бобошево','boboshevo',33,'2660','+7046'),
	(25,'Божурище','bozhurische',33,'2227','+2933'),
	(26,'Бойчиновци','boychinovtsi',33,'3430','+9513'),
	(27,'Болярово','bolyarovo',33,'8720','+4741'),
	(28,'Борово','borovo',33,'7174','+8140'),
	(29,'Ботевград','botevgrad',33,'2140','+723'),
	(30,'Брацигово','bratsigovo',33,'4579','+3552'),
	(31,'Брегово','bregovo',33,'3790','+9312'),
	(32,'Брезник','breznik',33,'2360','+7751'),
	(33,'Брезово','brezovo',33,'4160','+3191'),
	(34,'Брусарци','brusartsi',33,'3680','+9783'),
	(35,'Бургас','burgas',33,'8000','+56'),
	(36,'Бухово','buhovo',33,'1830','+2994'),
	(37,'Българово','bulgarovo',33,'8110','+5915'),
	(38,'Бяла','byala',33,'9101','+5143'),
	(39,'Бяла','byala-1',33,'7100','+817'),
	(40,'Бяла Слатина','byala-slatina',33,'3200','+915'),
	(41,'Бяла Черква','byala-cherkva',33,'5220','+6134'),
	(42,'Варна','varna',33,'9000','+52'),
	(43,'Велики Преслав','veliki-preslav',33,'9850','+538'),
	(44,'Велико Търново','veliko-turnovo',33,'5000','+62'),
	(45,'Велинград','velingrad',33,'4600','+359'),
	(46,'Ветово','vetovo',33,'7080','+8161'),
	(47,'Ветрен','vetren',33,'4480','+3584'),
	(48,'Видин','vidin',33,'3700','+94'),
	(49,'Враца','vratsa',33,'3000','+92'),
	(50,'Вълчедръм','vulchedrum',33,'3650','+9744'),
	(51,'Вълчи дол','vulchi-dol',33,'9280','+5131'),
	(52,'Върбица','vurbitsa',33,'9870','+5391'),
	(53,'Вършец','vurshets',33,'3540','+9527'),
	(54,'Габрово','gabrovo',33,'5300','+66'),
	(55,'Генерал Тошево','general-toshevo',33,'9500','+5731'),
	(56,'Главиница','glavinitsa',33,'7630','+8636'),
	(57,'Глоджево','glodzhevo',33,'7040','+8184'),
	(58,'Годеч','godech',33,'2240','+729'),
	(59,'Горна Оряховица','gorna-oryahovitsa',33,'5100','+618'),
	(60,'Гоце Делчев','gotse-delchev',33,'2900','+751'),
	(61,'Грамада','gramada',33,'3830','+9337'),
	(62,'Гулянци','gulyantsi',33,'5960','+6561'),
	(63,'Гурково','gurkovo',33,'6199','+4331'),
	(64,'Гълъбово','gulubovo',33,'6280','+418'),
	(65,'Две могили','dve-mogili',33,'7150','+8141'),
	(66,'Дебелец','debelets',33,'5030','+6117'),
	(67,'Девин','devin',33,'4800','+3041'),
	(68,'Девня','devnya',33,'9160','+519'),
	(69,'Джебел','dzhebel',33,'6850','+3632'),
	(70,'Димитровград','dimitrovgrad',33,'6400','+391'),
	(71,'Димово','dimovo',33,'3750','+9341'),
	(72,'Добринище','dobrinische',33,'2777','+7447'),
	(73,'Добрич','dobrich',33,'9300','+58'),
	(74,'Долна баня','dolna-banya',33,'2040','+7120'),
	(75,'Долна Митрополия','dolna-mitropoliya',33,'5855','+6552'),
	(76,'Долна Оряховица','dolna-oryahovitsa',33,'5130','+6173'),
	(77,'Долни Дъбник','dolni-dubnik',33,'5870','+6514'),
	(78,'Долни чифлик','dolni-chiflik',33,'9120','+5142'),
	(79,'Доспат','dospat',33,'4831','+3045'),
	(80,'Драгоман','dragoman',33,'2210','+7172'),
	(81,'Дряново','dryanovo',33,'5370','+676'),
	(82,'Дулово','dulovo',33,'7650','+855'),
	(83,'Дунавци','dunavtsi',33,'3740','+9314'),
	(84,'Дупница','dupnitsa',33,'2600','+701'),
	(85,'Дългопол','dulgopol',33,'9250','+517'),
	(86,'Елена','elena',33,'5070','+6151'),
	(87,'Елин Пелин','elin-pelin',33,'2100','+725'),
	(88,'Елхово','elhovo',33,'8700','+478'),
	(89,'Етрополе','etropole',33,'2180','+712'),
	(90,'Завет','zavet',33,'7330','+8442'),
	(91,'Земен','zemen',33,'2440','+7741'),
	(92,'Златарица','zlataritsa',33,'5090','+6197'),
	(93,'Златица','zlatitsa',33,'2080','+728'),
	(94,'Златоград','zlatograd',33,'4980','+3071'),
	(95,'Ивайловград','ivaylovgrad',33,'6570','+3661'),
	(96,'Игнатиево','ignatievo',33,'9143','+5119'),
	(97,'Искър','iskur',33,'5868','+6516'),
	(98,'Исперих','isperih',33,'7400','+835'),
	(99,'Ихтиман','ihtiman',33,'2050','+724'),
	(100,'Каблешково','kableshkovo',33,'8210','+5968'),
	(101,'Каварна','kavarna',33,'9650','+570'),
	(102,'Казанлък','kazanluk',33,'6100','+431'),
	(103,'Калофер','kalofer',33,'4370','+3133'),
	(104,'Камено','kameno',33,'8120','+5515'),
	(105,'Каолиново','kaolinovo',33,'9960','+5361'),
	(106,'Карлово','karlovo',33,'4300','+335'),
	(107,'Карнобат','karnobat',33,'8400','+559'),
	(108,'Каспичан','kaspichan',33,'9930','+5327'),
	(109,'Кермен','kermen',33,'8870','+4516'),
	(110,'Килифарево','kilifarevo',33,'5050','+6114'),
	(111,'Китен','kiten',33,'8183','+550'),
	(112,'Клисура','klisura',33,'4341','+3137'),
	(113,'Кнежа','knezha',33,'5835','+9132'),
	(114,'Козлодуй','kozloduy',33,'3320','+973'),
	(115,'Койнаре','koynare',33,'5986','+6573'),
	(116,'Копривщица','koprivschitsa',33,'2077','+7184'),
	(117,'Костандово','kostandovo',33,'4644','+3544'),
	(118,'Костенец','kostenets',33,'2030','+720'),
	(119,'Костинброд','kostinbrod',33,'2230','+721'),
	(120,'Котел','kotel',33,'8970','+453'),
	(121,'Кочериново','kocherinovo',33,'2640','+7053'),
	(122,'Кресна','kresna',33,'2840','+7433'),
	(123,'Криводол','krivodol',33,'3060','+9117'),
	(124,'Кричим','krichim',33,'4220','+3145'),
	(125,'Крумовград','krumovgrad',33,'6900','+360'),
	(126,'Кубрат','kubrat',33,'7300','+838'),
	(127,'Куклен','kuklen',33,'4101','+3115'),
	(128,'Кула','kula',33,'3800','+938'),
	(129,'Кърджали','kurdzhali',33,'6600','+361'),
	(130,'Кюстендил','kyustendil',33,'2500','+78'),
	(131,'Левски','levski',33,'5900','+650'),
	(132,'Летница','letnitsa',33,'5570','+6941'),
	(133,'Ловеч','lovech',33,'5500','+68'),
	(134,'Лозница','loznitsa',33,'7290','+8475'),
	(135,'Лом','lom',33,'3600','+971'),
	(136,'Луковит','lukovit',33,'5770','+697'),
	(137,'Лъки','luki',33,'4241','+3052'),
	(138,'Любимец','lyubimets',33,'6550','+3751'),
	(139,'Лясковец','lyaskovets',33,'5140','+619'),
	(140,'Мадан','madan',33,'4900','+308'),
	(141,'Маджарово','madzharovo',33,'6480','+3720'),
	(142,'Малко Търново','malko-turnovo',33,'8162','+5952'),
	(143,'Мартен','marten',33,'7058','+8117'),
	(144,'Мездра','mezdra',33,'3100','+910'),
	(145,'Мелник','melnik',33,'2820','+7437'),
	(146,'Меричлери','merichleri',33,'6430','+3921'),
	(147,'Мизия','miziya',33,'3330','+9161'),
	(148,'Момин проход','momin-prohod',33,'2035','+7142'),
	(149,'Момчилград','momchilgrad',33,'6800','+363'),
	(150,'Монтана','montana',33,'3400','+96'),
	(151,'Мъглиж','muglizh',33,'6180','+4321'),
	(152,'Неделино','nedelino',33,'4990','+3072'),
	(153,'Несебър','nesebur',33,'8230','+554'),
	(154,'Николаево','nikolaevo',33,'6190','+4330'),
	(155,'Никопол','nikopol',33,'5940','+6541'),
	(156,'Нова Загора','nova-zagora',33,'8900','+457'),
	(157,'Нови Искър','novi-iskur',33,'1280','+2991'),
	(158,'Нови пазар','novi-pazar',33,'9900','+537'),
	(159,'Обзор','obzor',33,'8250','+556'),
	(160,'Омуртаг','omurtag',33,'7900','+605'),
	(161,'Опака','opaka',33,'7840','+6039'),
	(162,'Оряхово','oryahovo',33,'3300','+9171'),
	(163,'Павел баня','pavel-banya',33,'6155','+4361'),
	(164,'Павликени','pavlikeni',33,'5200','+610'),
	(165,'Пазарджик','pazardzhik',33,'4400','+34'),
	(166,'Панагюрище','panagyurische',33,'4500','+357'),
	(167,'Перник','pernik',33,'2300','+76'),
	(168,'Перущица','peruschitsa',33,'4225','+3143'),
	(169,'Петрич','petrich',33,'2850','+745'),
	(170,'Пещера','peschera',33,'4550','+350'),
	(171,'Пирдоп','pirdop',33,'2070','+7181'),
	(172,'Плачковци','plachkovtsi',33,'5360','+6770'),
	(173,'Плевен','pleven',33,'5800','+64'),
	(174,'Плиска','pliska',33,'9920','+5323'),
	(175,'Пловдив','plovdiv',33,'4000','+32'),
	(176,'Полски Тръмбеш','polski-trumbesh',33,'5180','+6141'),
	(177,'Поморие','pomorie',33,'8200','+596'),
	(178,'Попово','popovo',33,'7800','+608'),
	(179,'Пордим','pordim',33,'5898','+6513'),
	(180,'Правец','pravets',33,'2161','+7133'),
	(181,'Приморско','primorsko',33,'8180','+5561'),
	(182,'Провадия','provadiya',33,'9200','+518'),
	(183,'Първомай','purvomay',33,'4270','+336'),
	(184,'Раднево','radnevo',33,'6260','+417'),
	(185,'Радомир','radomir',33,'2400','+777'),
	(186,'Разград','razgrad',33,'7200','+84'),
	(187,'Разлог','razlog',33,'2760','+747'),
	(188,'Ракитово','rakitovo',33,'4640','+3542'),
	(189,'Раковски','rakovski',33,'4150','+3151'),
	(190,'Рила','rila',33,'2630','+7054'),
	(191,'Роман','roman',33,'3130','+9123'),
	(192,'Рудозем','rudozem',33,'4960','+306'),
	(193,'Русе','ruse',33,'7000','+82'),
	(194,'Садово','sadovo',33,'4122','+3118'),
	(195,'Самоков','samokov',33,'2000','+722'),
	(196,'Сандански','sandanski',33,'2800','+746'),
	(197,'Сапарева баня','sapareva-banya',33,'2650','+707'),
	(198,'Свети Влас','sveti-vlas',33,'8256','+554'),
	(199,'Свиленград','svilengrad',33,'6500','+379'),
	(200,'Свищов','svischov',33,'5250','+631'),
	(201,'Своге','svoge',33,'2260','+726'),
	(202,'Севлиево','sevlievo',33,'5400','+675'),
	(203,'Сеново','senovo',33,'7038','+8185'),
	(204,'Септември','septemvri',33,'4490','+3561'),
	(205,'Силистра','silistra',33,'7500','+86'),
	(206,'Симеоновград','simeonovgrad',33,'6490','+3781'),
	(207,'Симитли','simitli',33,'2730','+7481'),
	(208,'Славяново','slavyanovo',33,'5840','+6515'),
	(209,'Сливен','sliven',33,'8800','+44'),
	(210,'Сливница','slivnitsa',33,'2200','+727'),
	(211,'Сливо поле','slivo-pole',33,'7060','+8131'),
	(212,'Смолян','smolyan',33,'4700','+301'),
	(213,'Смядово','smyadovo',33,'9820','+5351'),
	(214,'Созопол','sozopol',33,'8130','+550'),
	(215,'Сопот','sopot',33,'4330','+3134'),
	(216,'София','sofiya',33,'1000','+2'),
	(217,'Средец','sredets',33,'8300','+5551'),
	(218,'Стамболийски','stamboliyski',33,'4210','+339'),
	(219,'Стара Загора','stara-zagora',33,'6000','+42'),
	(220,'Стражица','strazhitsa',33,'5150','+6161'),
	(221,'Стралджа','straldzha',33,'8680','+4761'),
	(222,'Стрелча','strelcha',33,'4530','+3532'),
	(223,'Суворово','suvorovo',33,'9170','+5153'),
	(224,'Сунгурларе','sungurlare',33,'8470','+5571'),
	(225,'Сухиндол','suhindol',33,'5240','+6136'),
	(226,'Съединение','suedinenie',33,'4190','+3181'),
	(227,'Сърница','surnitsa',33,'4633','+3547'),
	(228,'Твърдица','tvurditsa',33,'8890','+454'),
	(229,'Тервел','tervel',33,'9450','+5751'),
	(230,'Тетевен','teteven',33,'5700','+678'),
	(231,'Тополовград','topolovgrad',33,'6560','+470'),
	(232,'Троян','troyan',33,'5600','+670'),
	(233,'Трън','trun',33,'2460','+7731'),
	(234,'Тръстеник','trustenik',33,'5857','+6551'),
	(235,'Трявна','tryavna',33,'5350','+677'),
	(236,'Тутракан','tutrakan',33,'7600','+857'),
	(237,'Търговище','turgovische',33,'7700','+601'),
	(238,'Угърчин','ugurchin',33,'5580','+6931'),
	(239,'Хаджидимово','hadzhidimovo',33,'2933','+7528'),
	(240,'Харманли','harmanli',33,'6450','+373'),
	(241,'Хасково','haskovo',33,'6300','+38'),
	(242,'Хисаря','hisarya',33,'4180','+337'),
	(243,'Цар Калоян','tsar-kaloyan',33,'7280','+8314'),
	(244,'Царево','tsarevo',33,'8260','+590'),
	(245,'Чепеларе','chepelare',33,'4850','+3051'),
	(246,'Червен бряг','cherven-bryag',33,'5980','+659'),
	(247,'Черноморец','chernomorets',33,'8142','+550'),
	(248,'Чипровци','chiprovtsi',33,'3460','+9554'),
	(249,'Чирпан','chirpan',33,'6200','+416'),
	(250,'Шабла','shabla',33,'9680','+5743'),
	(251,'Шивачево','shivachevo',33,'8895','+4593'),
	(252,'Шипка','shipka',33,'6150','+4324'),
	(253,'Шумен','shumen',33,'9700','+54'),
	(254,'Ябланица','yablanitsa',33,'5750','+6991'),
	(255,'Якоруда','yakoruda',33,'2790','+7442'),
	(256,'Ямбол','yambol',33,'8600','+46');

/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table companies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table countries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `countries`;

CREATE TABLE `countries` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `iso` char(2) CHARACTER SET utf8 NOT NULL,
  `name` varchar(80) CHARACTER SET utf8 NOT NULL,
  `iso3` char(3) CHARACTER SET utf8 DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;

INSERT INTO `countries` (`id`, `iso`, `name`, `iso3`)
VALUES
	(1,'AF','AFGHANISTAN','AFG'),
	(2,'AL','ALBANIA','ALB'),
	(3,'DZ','ALGERIA','DZA'),
	(4,'AS','AMERICAN SAMOA','ASM'),
	(5,'AD','ANDORRA','AND'),
	(6,'AO','ANGOLA','AGO'),
	(7,'AI','ANGUILLA','AIA'),
	(8,'AQ','ANTARCTICA',NULL),
	(9,'AG','ANTIGUA AND BARBUDA','ATG'),
	(10,'AR','ARGENTINA','ARG'),
	(11,'AM','ARMENIA','ARM'),
	(12,'AW','ARUBA','ABW'),
	(13,'AU','AUSTRALIA','AUS'),
	(14,'AT','AUSTRIA','AUT'),
	(15,'AZ','AZERBAIJAN','AZE'),
	(16,'BS','BAHAMAS','BHS'),
	(17,'BH','BAHRAIN','BHR'),
	(18,'BD','BANGLADESH','BGD'),
	(19,'BB','BARBADOS','BRB'),
	(20,'BY','BELARUS','BLR'),
	(21,'BE','BELGIUM','BEL'),
	(22,'BZ','BELIZE','BLZ'),
	(23,'BJ','BENIN','BEN'),
	(24,'BM','BERMUDA','BMU'),
	(25,'BT','BHUTAN','BTN'),
	(26,'BO','BOLIVIA','BOL'),
	(27,'BA','BOSNIA AND HERZEGOVINA','BIH'),
	(28,'BW','BOTSWANA','BWA'),
	(29,'BV','BOUVET ISLAND',NULL),
	(30,'BR','BRAZIL','BRA'),
	(31,'IO','BRITISH INDIAN OCEAN TERRITORY',NULL),
	(32,'BN','BRUNEI DARUSSALAM','BRN'),
	(33,'BG','BULGARIA','BGR'),
	(34,'BF','BURKINA FASO','BFA'),
	(35,'BI','BURUNDI','BDI'),
	(36,'KH','CAMBODIA','KHM'),
	(37,'CM','CAMEROON','CMR'),
	(38,'CA','CANADA','CAN'),
	(39,'CV','CAPE VERDE','CPV'),
	(40,'KY','CAYMAN ISLANDS','CYM'),
	(41,'CF','CENTRAL AFRICAN REPUBLIC','CAF'),
	(42,'TD','CHAD','TCD'),
	(43,'CL','CHILE','CHL'),
	(44,'CN','CHINA','CHN'),
	(45,'CX','CHRISTMAS ISLAND',NULL),
	(46,'CC','COCOS (KEELING) ISLANDS',NULL),
	(47,'CO','COLOMBIA','COL'),
	(48,'KM','COMOROS','COM'),
	(49,'CG','CONGO','COG'),
	(50,'CD','CONGO, THE DEMOCRATIC REPUBLIC OF THE','COD'),
	(51,'CK','COOK ISLANDS','COK'),
	(52,'CR','COSTA RICA','CRI'),
	(53,'CI','COTE D\'IVOIRE','CIV'),
	(54,'HR','CROATIA','HRV'),
	(55,'CU','CUBA','CUB'),
	(56,'CY','CYPRUS','CYP'),
	(57,'CZ','CZECH REPUBLIC','CZE'),
	(58,'DK','DENMARK','DNK'),
	(59,'DJ','DJIBOUTI','DJI'),
	(60,'DM','DOMINICA','DMA'),
	(61,'DO','DOMINICAN REPUBLIC','DOM'),
	(62,'EC','ECUADOR','ECU'),
	(63,'EG','EGYPT','EGY'),
	(64,'SV','EL SALVADOR','SLV'),
	(65,'GQ','EQUATORIAL GUINEA','GNQ'),
	(66,'ER','ERITREA','ERI'),
	(67,'EE','ESTONIA','EST'),
	(68,'ET','ETHIOPIA','ETH'),
	(69,'FK','FALKLAND ISLANDS (MALVINAS)','FLK'),
	(70,'FO','FAROE ISLANDS','FRO'),
	(71,'FJ','FIJI','FJI'),
	(72,'FI','FINLAND','FIN'),
	(73,'FR','FRANCE','FRA'),
	(74,'GF','FRENCH GUIANA','GUF'),
	(75,'PF','FRENCH POLYNESIA','PYF'),
	(76,'TF','FRENCH SOUTHERN TERRITORIES',NULL),
	(77,'GA','GABON','GAB'),
	(78,'GM','GAMBIA','GMB'),
	(79,'GE','GEORGIA','GEO'),
	(80,'DE','GERMANY','DEU'),
	(81,'GH','GHANA','GHA'),
	(82,'GI','GIBRALTAR','GIB'),
	(83,'GR','GREECE','GRC'),
	(84,'GL','GREENLAND','GRL'),
	(85,'GD','GRENADA','GRD'),
	(86,'GP','GUADELOUPE','GLP'),
	(87,'GU','GUAM','GUM'),
	(88,'GT','GUATEMALA','GTM'),
	(89,'GN','GUINEA','GIN'),
	(90,'GW','GUINEA-BISSAU','GNB'),
	(91,'GY','GUYANA','GUY'),
	(92,'HT','HAITI','HTI'),
	(93,'HM','HEARD ISLAND AND MCDONALD ISLANDS',NULL),
	(94,'VA','HOLY SEE (VATICAN CITY STATE)','VAT'),
	(95,'HN','HONDURAS','HND'),
	(96,'HK','HONG KONG','HKG'),
	(97,'HU','HUNGARY','HUN'),
	(98,'IS','ICELAND','ISL'),
	(99,'IN','INDIA','IND'),
	(100,'ID','INDONESIA','IDN'),
	(101,'IR','IRAN, ISLAMIC REPUBLIC OF','IRN'),
	(102,'IQ','IRAQ','IRQ'),
	(103,'IE','IRELAND','IRL'),
	(104,'IL','ISRAEL','ISR'),
	(105,'IT','ITALY','ITA'),
	(106,'JM','JAMAICA','JAM'),
	(107,'JP','JAPAN','JPN'),
	(108,'JO','JORDAN','JOR'),
	(109,'KZ','KAZAKHSTAN','KAZ'),
	(110,'KE','KENYA','KEN'),
	(111,'KI','KIRIBATI','KIR'),
	(112,'KP','KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF','PRK'),
	(113,'KR','KOREA, REPUBLIC OF','KOR'),
	(114,'KW','KUWAIT','KWT'),
	(115,'KG','KYRGYZSTAN','KGZ'),
	(116,'LA','LAO PEOPLE\'S DEMOCRATIC REPUBLIC','LAO'),
	(117,'LV','LATVIA','LVA'),
	(118,'LB','LEBANON','LBN'),
	(119,'LS','LESOTHO','LSO'),
	(120,'LR','LIBERIA','LBR'),
	(121,'LY','LIBYAN ARAB JAMAHIRIYA','LBY'),
	(122,'LI','LIECHTENSTEIN','LIE'),
	(123,'LT','LITHUANIA','LTU'),
	(124,'LU','LUXEMBOURG','LUX'),
	(125,'MO','MACAO','MAC'),
	(126,'MK','MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF','MKD'),
	(127,'MG','MADAGASCAR','MDG'),
	(128,'MW','MALAWI','MWI'),
	(129,'MY','MALAYSIA','MYS'),
	(130,'MV','MALDIVES','MDV'),
	(131,'ML','MALI','MLI'),
	(132,'MT','MALTA','MLT'),
	(133,'MH','MARSHALL ISLANDS','MHL'),
	(134,'MQ','MARTINIQUE','MTQ'),
	(135,'MR','MAURITANIA','MRT'),
	(136,'MU','MAURITIUS','MUS'),
	(137,'YT','MAYOTTE',NULL),
	(138,'MX','MEXICO','MEX'),
	(139,'FM','MICRONESIA, FEDERATED STATES OF','FSM'),
	(140,'MD','MOLDOVA, REPUBLIC OF','MDA'),
	(141,'MC','MONACO','MCO'),
	(142,'MN','MONGOLIA','MNG'),
	(143,'MS','MONTSERRAT','MSR'),
	(144,'MA','MOROCCO','MAR'),
	(145,'MZ','MOZAMBIQUE','MOZ'),
	(146,'MM','MYANMAR','MMR'),
	(147,'NA','NAMIBIA','NAM'),
	(148,'NR','NAURU','NRU'),
	(149,'NP','NEPAL','NPL'),
	(150,'NL','NETHERLANDS','NLD'),
	(151,'AN','NETHERLANDS ANTILLES','ANT'),
	(152,'NC','NEW CALEDONIA','NCL'),
	(153,'NZ','NEW ZEALAND','NZL'),
	(154,'NI','NICARAGUA','NIC'),
	(155,'NE','NIGER','NER'),
	(156,'NG','NIGERIA','NGA'),
	(157,'NU','NIUE','NIU'),
	(158,'NF','NORFOLK ISLAND','NFK'),
	(159,'MP','NORTHERN MARIANA ISLANDS','MNP'),
	(160,'NO','NORWAY','NOR'),
	(161,'OM','OMAN','OMN'),
	(162,'PK','PAKISTAN','PAK'),
	(163,'PW','PALAU','PLW'),
	(164,'PS','PALESTINIAN TERRITORY, OCCUPIED',NULL),
	(165,'PA','PANAMA','PAN'),
	(166,'PG','PAPUA NEW GUINEA','PNG'),
	(167,'PY','PARAGUAY','PRY'),
	(168,'PE','PERU','PER'),
	(169,'PH','PHILIPPINES','PHL'),
	(170,'PN','PITCAIRN','PCN'),
	(171,'PL','POLAND','POL'),
	(172,'PT','PORTUGAL','PRT'),
	(173,'PR','PUERTO RICO','PRI'),
	(174,'QA','QATAR','QAT'),
	(175,'RE','REUNION','REU'),
	(176,'RO','ROMANIA','ROM'),
	(177,'RU','RUSSIAN FEDERATION','RUS'),
	(178,'RW','RWANDA','RWA'),
	(179,'SH','SAINT HELENA','SHN'),
	(180,'KN','SAINT KITTS AND NEVIS','KNA'),
	(181,'LC','SAINT LUCIA','LCA'),
	(182,'PM','SAINT PIERRE AND MIQUELON','SPM'),
	(183,'VC','SAINT VINCENT AND THE GRENADINES','VCT'),
	(184,'WS','SAMOA','WSM'),
	(185,'SM','SAN MARINO','SMR'),
	(186,'ST','SAO TOME AND PRINCIPE','STP'),
	(187,'SA','SAUDI ARABIA','SAU'),
	(188,'SN','SENEGAL','SEN'),
	(189,'CS','SERBIA AND MONTENEGRO',NULL),
	(190,'SC','SEYCHELLES','SYC'),
	(191,'SL','SIERRA LEONE','SLE'),
	(192,'SG','SINGAPORE','SGP'),
	(193,'SK','SLOVAKIA','SVK'),
	(194,'SI','SLOVENIA','SVN'),
	(195,'SB','SOLOMON ISLANDS','SLB'),
	(196,'SO','SOMALIA','SOM'),
	(197,'ZA','SOUTH AFRICA','ZAF'),
	(198,'GS','SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',NULL),
	(199,'ES','SPAIN','ESP'),
	(200,'LK','SRI LANKA','LKA'),
	(201,'SD','SUDAN','SDN'),
	(202,'SR','SURINAME','SUR'),
	(203,'SJ','SVALBARD AND JAN MAYEN','SJM'),
	(204,'SZ','SWAZILAND','SWZ'),
	(205,'SE','SWEDEN','SWE'),
	(206,'CH','SWITZERLAND','CHE'),
	(207,'SY','SYRIAN ARAB REPUBLIC','SYR'),
	(208,'TW','TAIWAN, PROVINCE OF CHINA','TWN'),
	(209,'TJ','TAJIKISTAN','TJK'),
	(210,'TZ','TANZANIA, UNITED REPUBLIC OF','TZA'),
	(211,'TH','THAILAND','THA'),
	(212,'TL','TIMOR-LESTE',NULL),
	(213,'TG','TOGO','TGO'),
	(214,'TK','TOKELAU','TKL'),
	(215,'TO','TONGA','TON'),
	(216,'TT','TRINIDAD AND TOBAGO','TTO'),
	(217,'TN','TUNISIA','TUN'),
	(218,'TR','TURKEY','TUR'),
	(219,'TM','TURKMENISTAN','TKM'),
	(220,'TC','TURKS AND CAICOS ISLANDS','TCA'),
	(221,'TV','TUVALU','TUV'),
	(222,'UG','UGANDA','UGA'),
	(223,'UA','UKRAINE','UKR'),
	(224,'AE','UNITED ARAB EMIRATES','ARE'),
	(225,'GB','UNITED KINGDOM','GBR'),
	(226,'US','UNITED STATES','USA'),
	(227,'UM','UNITED STATES MINOR OUTLYING ISLANDS',NULL),
	(228,'UY','URUGUAY','URY'),
	(229,'UZ','UZBEKISTAN','UZB'),
	(230,'VU','VANUATU','VUT'),
	(231,'VE','VENEZUELA','VEN'),
	(232,'VN','VIET NAM','VNM'),
	(233,'VG','VIRGIN ISLANDS, BRITISH','VGB'),
	(234,'VI','VIRGIN ISLANDS, U.S.','VIR'),
	(235,'WF','WALLIS AND FUTUNA','WLF'),
	(236,'EH','WESTERN SAHARA','ESH'),
	(237,'YE','YEMEN','YEM'),
	(238,'ZM','ZAMBIA','ZMB'),
	(239,'ZW','ZIMBABWE','ZWE');

/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table employees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `phone` varchar(255) DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `company_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table order_items
# ------------------------------------------------------------

DROP TABLE IF EXISTS `order_items`;

CREATE TABLE `order_items` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `unit_price` decimal(10,2) NOT NULL,
  `unit_discount` decimal(10,2) DEFAULT NULL,
  `unit_quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table orders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `shipping_address_id` int(11) NOT NULL,
  `billing_address_id` int(11) DEFAULT NULL,
  `total_price` decimal(10,2) NOT NULL,
  `notes` text,
  `ip` int(10) unsigned DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table product_discounts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `product_discounts`;

CREATE TABLE `product_discounts` (
  `product_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `discount` decimal(10,2) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`product_id`,`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL,
  `description` text,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
